# !/usr/bin/env python
# -*- coding: utf8 -*-

import sys, getopt, time, threading, requests, queue, csv, json, codecs, timeit

def do_get(uri, hdrs):

    start = timeit.default_timer()
    
    resp = requests.get(uri, headers = hdrs)
    
    end = timeit.default_timer()
    
    duration = end - start
    
    return resp, duration

def do_post(uri, hdrs, bdy):

    start = timeit.default_timer()
    
    resp = requests.post(uri, headers = hdrs, data = bdy)
    
    end = timeit.default_timer()
    
    duration = end - start
    
    return resp, duration

def iterate_get(q, uri, thread_index, iterations, delay_per_iteration):
    
    headers = { 'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.101 Safari/537.36' }
    
    body = ""
    
    for iteration in range(iterations):
    
        print("running Thread {}, Iteration {}".format(thread_index, iteration))
        
        if delay_per_iteration > 0:
            time.sleep(delay_per_iteration)
        
        response, duration = do_get(uri, headers)

        q.put({ 'thread_index': thread_index, 'iteration': iteration, 'verb': 'get', 'uri': uri, 'request_headers': json.dumps(dict(headers)), 'request_body': body, 'status': response.status_code, 'response_headers': json.dumps(dict(response.headers)), 'response _content': str(response.content), 'duration': duration })
    
def iterate_post(q, uri, thread_index, iterations, delay_per_iteration):

    headers, body = get_post_request()

    for iteration in range(iterations):
    
        print("running Thread {}, Iteration {}".format(thread_index, iteration))
        
        if delay_per_iteration > 0:
            time.sleep(delay_per_iteration)
        
        response, duration = do_post(uri, headers, body)

        q.put({ 'thread_index': thread_index, 'iteration': iteration, 'verb': 'post', 'uri': uri, 'request_headers': json.dumps(dict(headers)), 'request_body': body, 'status': response.status_code, 'response_headers': json.dumps(dict(response.headers)), 'response _content': str(response.content), 'duration': duration })
   
def load_post_body_from_file(path):

    print("load_body")

    body = ''
    
    with open(path, 'r') as f:
        body = f.read()
    
    return body
       
def get_post_request(path = None):

    # { "content-type": "application/soap+xml" }
    headers = { 'content-type': 'text/xml', 
                'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.101 Safari/537.36' }
    
    body = ''
                
    if not path is None:
        body = load_post_body_from_file(path)
    else:        
        body = '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Body><GetHTML xmlns="http://www.webserviceX.NET"><RSSURL>http://feeds.reuters.com/news/artsculture</RSSURL></GetHTML></soap:Body></soap:Envelope>'

    return headers, body

def dump_json(q, path):
    
    datalist = []
    
    while not q.empty():
        data = q.get()
        datalist.append(data)
        
    with open(path, 'wb') as f:
        json.dump(datalist, codecs.getwriter('utf-8')(f), ensure_ascii = False)
        
def dump_csv(q, path):
       
    with open(path, 'w', newline = '', encoding = 'utf-8') as f:
    
        writer = csv.writer(f)

        writer.writerow(["thread_index", "iteration", "verb", "uri", "request_headers", "request_body", "status", "response_headers", "response_content", "duration"])

        row = []

        while not q.empty():
        
            data = q.get()
            
            for key in data.keys():
                row.append(data[key])
        
            writer.writerow(row)
                
            row.clear()
                
def dump_q(q, format, path):
    if format == "csv":
        dump_csv(q, path)
    elif format == "json":
        dump_json(q, path)
             
def print_usage():
    print("Usage: test.py [-v | --verb <'post' or 'get' (for now lol)>] [-u | --uri <the uri to get or post to>] [-t | --threads <thread count>] [-i | --iterations <iterations per thread>] [-d | --delay <delay per iteration (seconds)>] [-o | --outfile <output file>] [-f | --format <output file format ('csv' or 'json')>")   
    print("Defaults are: -v post -t 1 -i 1 -d 0 -o ./results.csv -f csv")
    print("-h or --help displays this help text.")
     
def get_args(argv):

    try:
        opts, args = getopt.getopt(argv, "hv:t:i:d:o:f:", ["help", "verb=", "threads=", "iterations=", "delay=", "outfile=", "format="])
        return opts, args
    except getopt.GetoptError as goe:
        print(str(goe))
        print_usage()
        sys.exit(2)

def get_func(verb):

    if verb == "get":
        func = iterate_get
    elif verb == "post":
        func = iterate_post
    else:
        print_usage()
        sys.exit()
        
    return func
        
def main(argv):   

    verb = "get"
    uri = None
    thread_count = 1
    iterations_per_thread = 1
    delay_per_iteration = 0
    outfile = "./results.csv"
    outfile_format = "csv"

    opts, args = get_args(argv)

    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print_usage()
            sys.exit()
        elif opt in ("-v", "--verb"):
            verb = arg.lower()
        elif opt in ("-u", "--uri"):
            uri = arg
        elif opt in ("-t", "--threads"):
            thread_count = int(arg)
        elif opt in ("-i", "--iterations"):
            iterations_per_thread = int(arg)
        elif opt in ("-d", "--delay"):
            delay_per_iteration = int(arg)
        elif opt in ("-o", "--outfile"):
            outfile = arg
        elif opt in ("-f", "--format"):
            outfile_format = arg.lower()

    if uri is None:
        print("uri is required but has been defaulted to..")
        uri = "http://example.com"
        print(uri)
        verb = 'get'
        print("and verb is defaulted to get!")

    print("verb = {}".format(verb))
    print("uri = {}".format(uri))
    print("thread_count = {}".format(thread_count))
    print("iterations_per_thread = {}".format(iterations_per_thread))
    print("delay_per_iteration = {}".format(delay_per_iteration))
    print("outfile = {}".format(outfile))
    print("outfile_format = {}".format(outfile_format))
    
    q = queue.Queue()
    
    func = get_func
  
    threads = [threading.Thread(target = func, 
                                args=(q, 
                                uri, 
                                t, 
                                iterations_per_thread, 
                                delay_per_iteration)) 
               for t in range(thread_count)]

    for thread in threads:
        thread.start()
        
    for thread in threads:
        thread.join()

    print("saving...")
    
    dump_q(q, outfile_format, outfile)
        
    print("Done.")
        
if __name__ == "__main__":
    main(sys.argv[1:])
